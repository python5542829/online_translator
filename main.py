import requests
import bs4
import argparse
from requests.exceptions import HTTPError
from msg import *

LANGUAGES = {
    '1': 'arabic',
    '2': 'german',
    '3': 'english',
    '4': 'spanish',
    '5': 'french',
    '6': 'hebrew',
    '7': 'japanese',
    '8': 'dutch',
    '9': 'polish',
    '10': 'portuguese',
    '11': 'romanian',
    '12': 'russian',
    '13': 'turkish'
}


def is_valid_language(languages, lang) -> bool:
    if lang not in languages.values():
        return False
    return True


def request_reverso(source, dest, word):
    url = f'https://context.reverso.net/translation/{source}-{dest}/{word}'
    headers = {'User-Agent': 'Mozilla/5.0'}
    session = requests.Session()
    response = session.get(url, headers=headers)

    if response == None:
        raise HTTPError('Something wrong with your internet connection')
    if response.status_code != requests.codes.ok:
        raise ValueError(f'Sorry, unable to find {word}')
    return response


def get_soup(response):
    return bs4.BeautifulSoup(response.text, 'html.parser')


def get_translations(reverso_soup):
    result = []
    # get the translations
    tags = reverso_soup.select('#translations-content > .translation')

    for t in tags:
        result.append(t.get_text().strip())

    return result


def get_examples(reverso_soup):
    result = []
    # get examples
    tags = reverso_soup.select('.example > .ltr')
    for t in tags:
        result.append(t.get_text().strip())
    return result


def get_arabic_examples(reverso_soup):
    result = []
    # get examples
    tags = reverso_soup.select('.example > .ltr')
    tags_ar = reverso_soup.select('.example > .rtl')
    for t, a in zip(tags, tags_ar):
        result.append(t.get_text().strip())
        result.append(a.get_text().strip())
    return result


def save_translation(from_lang, to_lang, user_word, mode='w'):
    # open file
    file = open(f'{user_word}.txt', mode, encoding='utf-8')

    # request webpage
    response = request_reverso(from_lang, to_lang, user_word)

    # parse html
    reverso_soup = get_soup(response)

    # get and save translations
    translations_msg = f'{to_lang.capitalize()} Translations:'
    file.write(translations_msg + '\n')

    translations = get_translations(reverso_soup)
    for t in translations:
        file.write(t + '\n')

    # get examples
    examples_msg = f'\n{to_lang.capitalize()} Examples:'
    file.write(examples_msg + '\n')

    if to_lang == 'arabic':
        examples = get_arabic_examples(reverso_soup)
    else:
        examples = get_examples(reverso_soup)
    for i in range(0, len(examples), 2):
        msg = f'{examples[i]}\n{examples[i + 1]}\n'
        file.write(msg + '\n')

    # close file
    file.close()


def save_all_translations(from_lang, user_word):
    for l in LANGUAGES.values():
        if l == 'arabic':
            save_translation(from_lang, l, user_word)
            continue
        save_translation(from_lang, l, user_word, mode='a')


def display_translations(user_word):
    with open(f'{user_word}.txt', 'r') as f:
        for line in f:
            print(line, end='')


if __name__ == '__main__':
    # initial message
    print(MSG0)
    for n, l in LANGUAGES.items():
        print(f'{n}. {l.capitalize()}')

    # Command-line arguments
    parser = argparse.ArgumentParser(description=MSG_DESCRIPTION)
    parser.add_argument('from_lang',
                        help='Specify the source language code'
                        )
    parser.add_argument('to_lang',
                        help='Specify the target language code'
                        )
    parser.add_argument('user_word',
                        help='Specify the word to be translated'
                        )
    parser.add_argument(
        '--version', action='version', version='%(prog)s 0.1.0'
    )
    args = parser.parse_args()

    try:
        # Validate variables
        from_lang = args.from_lang.strip()
        if not is_valid_language(LANGUAGES, from_lang):
            raise ValueError(f'Sorry, the program doesn\'t support {from_lang}')

        to_lang = args.to_lang.strip()
        if to_lang != 'all':
            if not is_valid_language(LANGUAGES, to_lang):
                raise ValueError(f'Sorry, the program doesn\'t support {to_lang}')

        user_word = args.user_word.strip()

        # display result
        if to_lang == 'all':
            save_all_translations(from_lang, user_word)
        else:
            save_translation(from_lang, to_lang, user_word)

        # display result
        display_translations(user_word)
    except ValueError as e:
        print(e)
    except HTTPError as e:
        print(e)
