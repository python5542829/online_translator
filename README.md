# Online translator
Script that translates a word from one language to another.
# Setup (linux)
- Inside the root directory (`online_translator`), install `requirements.txt`:
    ```shell
    python -m venv venv && \
    source venv/bin/activate && \
    pip install -r requirements.txt
    ```
- Run the program
    ```shell
    python main.py {from} {to} {word}
    # Ex.
    python main.py english spanish cat
    ```
